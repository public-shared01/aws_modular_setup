############################################################################################
#                                  Creating S3                                            #
############################################################################################
# module "s3-backend" {
#   source             = "../../modules/aws-s3"
#   name               = "tfstate-aws-s3-dev"
#   environment        = var.environment
# }

module "s3-attached-dev" {
  source             = "../../modules/aws-s3"
  name               = "s3-dev-data"
  environment        = var.environment
}

#########################################################################################
#                                  Creating VPC                                         #
#########################################################################################
module "vpc" {
  source             = "../../modules/vpc"
  vpc_cidr           = "172.16.0.0/16"
  public_subnets     = var.vpc_public_subnets
  private_subnets    = var.vpc_private_subnets
  vpc_name           = "development-vpc"
  availability_zones = var.azs
}

#########################################################################################
#                                  Creating EC2                                         #
#########################################################################################
module "ec2" {
  source             = "../../modules/ec2"
  create_ebs         = false
  create_eip         = true
  ec2_name           = "development-ec2"
  zone               = "ap-southeast-1a"
  instance_type      = "t3.micro"
  ami_id             = "ami-060e277c0d4cce553" #Ubuntu Server 24.04 LTS (HVM),EBS General Purpose (SSD) Volume Type.
  ebs_type           = "gp3"
  ebs_size           = 5
  private_ip         = "172.16.1.10"
  public_ip          = true
  subnet_id          = module.vpc.public_subnet_ids[0]
  sg_name            = ["icmp", "ssh", "http", "https"]
  vpc_public_subnets = var.vpc_public_subnets
  vpc_id             = module.vpc.vpc_id
  user_data          = ["ubuntu-cloud-config.yaml"]
}
