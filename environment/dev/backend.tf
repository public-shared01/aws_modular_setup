terraform {
  backend "s3" {
    bucket  = "tfstate-aws-s3-dev"
    key     = "state/dev/terraform.tfstate"
    region  = "us-west-2"
  }
}
