terraform {
  backend "s3" {
    bucket  = "tfstate-aws-s3-dev"
    key     = "state/prod/terraform.tfstate"
    region  = "us-west-2"
  }
}
