############################################################################################
#                                  Creating S3                                            #
############################################################################################
module "s3-attached-dev" {
  source             = "../../modules/aws-s3"
  name               = "s3-prod-data"
  environment        = var.environment
}

#########################################################################################
#                                  Creating VPC                                         #
#########################################################################################
module "vpc" {
  source             = "../../modules/vpc"
  vpc_cidr           = "172.20.0.0/16"
  public_subnets     = var.vpc_public_subnets
  private_subnets    = var.vpc_private_subnets
  vpc_name           = "prod-vpc"
  availability_zones = var.azs
}

#########################################################################################
#                                  Creating EC2                                         #
#########################################################################################
module "ec2" {
  source             = "../../modules/ec2"
  create_ebs         = false
  create_eip         = true
  ec2_name           = "prod-ec2-gw"
  zone               = var.azs[0]
  instance_type      = "t3.nano"
  ami_id             = "ami-0f8960d552d4d0de5" #Ubuntu Server 22.04 LTS (HVM),EBS General Purpose (SSD) Volume Type.
  ebs_type           = "gp3"
  ebs_size           = 5
  #private_ip         = "172.20.1.10"
  public_ip          = true
  subnet_id          = module.vpc.public_subnet_ids[0]
  sg_name            = ["icmp", "http", "https"]
  vpc_public_subnets = var.vpc_public_subnets
  vpc_id             = module.vpc.vpc_id
  user_data          = ["ubuntu-cloud-config-prod.yaml"]
}
