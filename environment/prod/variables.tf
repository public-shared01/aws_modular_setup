variable "region" {
    default = "ap-east-1"
}

variable "vpc_name" {
    type    = string
    default = "vpc"
}

variable "vpc_cidr" {
    type    = string
    default = false
}

variable "azs" {
    description = "Availability zones for VPC"
    type        = list(string)
    default     = ["ap-east-1a", "ap-east-1b"]
}

variable "vpc_public_subnets" {
    description = "Public subnets for VPC"
    type        = list(string)
    default     = ["172.20.1.0/24", "172.20.2.0/24"]
}

variable "vpc_private_subnets" {
    description = "Private subnets for VPC"
    type        = list(string)
    default     = ["172.20.10.0/24", "172.20.20.0/24"]
}

variable "vpc_tags" {
    description = "Tags to apply to resources created by VPC module"
    type        = map(string)
    default = {
      Terraform   = "true"
      Environment = "prod"
    }
}

variable "environment" {
    description = "Environment for VPC"
    type        = string
    default     = "prod"
}

variable "ami_id" {
    description = "AMI ID for EC2 instances"
    type        = string
    default     = "ami-08116b9957a259459"
}

variable "create_ebs" {
    description = "Whether to create an additional EBS volume"
    type        = bool
    default     = false
}

variable "create_eip" {
    description = "Whether to create an Elastic IP"
    type        = bool
    default     = false
}
