variable "vpc_id" {
  type = string
}

variable "sg_name" {
  type = list(string)
}

variable "create_sg" {
  description = "Whether to create security group and all rules"
  type        = bool
  default     = true
}

variable "tags" {
  description = "A mapping of tags to assign to security group"
  type        = map(string)
  default     = {}
}