resource "aws_security_group" "sg" {
  count             = var.sg_name != null ? length(var.sg_name) : 0
  vpc_id            = var.vpc_id
  name              = var.sg_name[count.index]
  tags              = merge({
    Name = var.sg_name[count.index]
  }, var.tags)
}

resource "aws_security_group_rule" "ingress" {
  count             = var.sg_name != null ? length(var.sg_name) : 0
  security_group_id = aws_security_group.sg[count.index].id
  from_port         = local.sg_rules[aws_security_group.sg[count.index].name][0].from_port
  protocol          = local.sg_rules[aws_security_group.sg[count.index].name][0].protocol
  to_port           = local.sg_rules[aws_security_group.sg[count.index].name][0].to_port
  type              = "ingress"
  cidr_blocks       = local.sg_rules[aws_security_group.sg[count.index].name][0].cidr_blocks
  ipv6_cidr_blocks  = []
  prefix_list_ids   = []
}

resource "aws_security_group_rule" "egress" {
  count             = var.sg_name != null ? length(var.sg_name) : 0
  security_group_id = aws_security_group.sg[count.index].id
  from_port         = local.sg_rules[aws_security_group.sg[count.index].name][0].from_port
  protocol          = local.sg_rules[aws_security_group.sg[count.index].name][0].protocol
  to_port           = local.sg_rules[aws_security_group.sg[count.index].name][0].to_port
  type              = "egress"
  cidr_blocks       = local.sg_rules[aws_security_group.sg[count.index].name][0].cidr_blocks
  ipv6_cidr_blocks  = []
  prefix_list_ids   = []
}
#########################################################################################
#                             Let's define some simple rules                            #
#########################################################################################
locals {
  sg_rules = {
    "icmp" = [{
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
    }],
    "custom_ssh" = [{
      from_port   = 4022
      to_port     = 4022
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }],
    "ssh" = [{
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }],
    "http" = [{
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
      }],
    "https" = [{
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
      }]
 }
}
