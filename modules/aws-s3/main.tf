resource "aws_s3_bucket" "aws_s3_bucket" {
  bucket        = var.name

  tags = {
    Name        = var.name
    Environment = var.environment
  }
}
