resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = var.vpc_name
  }
}

resource "aws_subnet" "public" {
  count                   = length(var.public_subnets)
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.public_subnets[count.index]
  availability_zone = var.availability_zones[count.index]
  map_public_ip_on_launch = true
    tags = {
        Name = "public-dev-subnet ${count.index + 1}"
    }
}

resource "aws_subnet" "private" {
  count      = length(var.private_subnets)
  vpc_id     = aws_vpc.vpc.id
  cidr_block = var.private_subnets[count.index]
  availability_zone = var.availability_zones[count.index]
  tags = {
    Name = "private-dev-subnet ${count.index + 1}"
  }
}

resource "aws_internet_gateway" "i-gw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "i-gw-${var.vpc_name}"
  }
}

resource "aws_route_table" "pubAccRT" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "public-access-RT-${var.vpc_name}"
  }
}

resource "aws_route_table_association" "public" {
  count          = length(var.public_subnets)
  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.pubAccRT.id
}

resource "aws_route" "route" {
  route_table_id         = aws_route_table.pubAccRT.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.i-gw.id
}
