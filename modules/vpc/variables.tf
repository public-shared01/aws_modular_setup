variable "vpc_name" {
    description = "The name of the custom VPC"
    type = string
}

variable "vpc_cidr" {
    description = "CIDR block for VPC"
    type        = string
}

variable "public_subnets" {
    description = "List of public subnets"
    type = list(string)
}

variable "private_subnets" {
    description = "List of private subnets"
    type = list(string)
}

variable "availability_zones" {
    description = "List of availability zones"
    type = list(string)
}
