resource "aws_ebs_volume" "ebs_volume" {
  count              = try(var.create_ebs ? 1 : 0)
  availability_zone  = var.zone
  size               = var.ebs_size
  type               = var.ebs_type
  tags = {
    Name = "ebs_volume"
  }
}

resource "aws_volume_attachment" "ebs_attach" {
  count              =  try(var.create_ebs ? 1 : 0)
  device_name        = "/dev/sdh"
  volume_id          = aws_ebs_volume.ebs_volume[count.index].id
  instance_id        = aws_instance.ec2_instance.id
}
