variable "vpc_id" {
    type = string
}

variable "zone" {
    type = string
}

variable "instance_type" {
    type = string
    default = "t2.micro"
}

variable "ebs_size" {
    type = number
    default = 8
}

variable "ami_id" {
    type = string
}

variable "ec2_name" {
    type = string
}

variable "ebs_type" {
    type = string
    default = false
}

variable "subnet_id" {
    type = string
}

variable "private_ip" {
    type = string
    default = false
}

variable "public_ip" {
    type = string
    default = false
}

variable "create_ebs" {
    type = bool
    default = false
}

variable "create_eip" {
    type = bool
    default = false
}

variable "vpc_public_subnets" {
    description = "Public subnets for VPC"
    type        = list(string)
}

variable "sg_name" {
    type = list(string)
}

variable "user_data" {
    type = list(string)
}