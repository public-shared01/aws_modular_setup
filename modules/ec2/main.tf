module "security_group" {
  source = "../security-group"
  vpc_id = var.vpc_id
  sg_name = var.sg_name
}

resource "aws_instance" "ec2_instance" {
  ami                = var.ami_id
  instance_type      = "t3.micro"
  availability_zone  = var.zone
  user_data = file("${path.module}/userdata-scripts/${var.user_data[0]}")
  network_interface {
    network_interface_id = aws_network_interface.ec2_network_interface.id
    device_index         = 0
  }
  tags = {
    Name = var.ec2_name
  }
}

resource "aws_network_interface" "ec2_network_interface" {
  subnet_id          = var.subnet_id
  private_ip         = var.private_ip
  security_groups    = module.security_group.sg_id
}

resource "aws_eip" "ec2_eip" {
  count              = try(var.create_eip ? 1 : 0)
  instance           = aws_instance.ec2_instance.id
  domain             = "vpc"
  tags = {
    Name = "eip-${var.ec2_name}"
  }
}
