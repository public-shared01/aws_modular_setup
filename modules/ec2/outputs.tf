output "instance_id" {
  value = aws_instance.ec2_instance.id
}

output "eip_address" {
  value = aws_eip.ec2_eip[0].public_ip
}