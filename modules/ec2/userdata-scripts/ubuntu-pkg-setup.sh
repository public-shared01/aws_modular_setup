#!/bin/sh
# Update the system
apt-get update -y
apt-get upgrade -y
# Install the necessary packages
apt-get install -y docker.io docker-compose docker-ce docker-ce-cli